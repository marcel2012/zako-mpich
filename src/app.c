#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

/********** Merge Function **********/
void Merge(int *a, int *b, int l, int m, int r) {
	int h, i, j, k;
	h = l;
	i = l;
	j = m + 1;

	while ((h <= m) && (j <= r)) {
		if (a[h] <= a[j]) {
			b[i] = a[h];
			h++;
		}
		else {
			b[i] = a[j];
			j++;
		}
		i++;
	}

	if (m < h) {
		for (k = j; k <= r; k++) {
			b[i] = a[k];
			i++;
		}
	}
	else {
		for (k = h; k <= m; k++) {
			b[i] = a[k];
			i++;
		}
	}

	for (k = l; k <= r; k++) {
		a[k] = b[k];
	}
}

/********** Recursive Merge Function **********/
void MergeSort(int *a, int *b, int l, int r) {
	int m;

	if (l < r) {
		m = (l + r) / 2;

		MergeSort(a, b, l, m);
		MergeSort(a, b, (m + 1), r);
		Merge(a, b, l, m, r);
	}
}

/********** Final merging of any size chunks function **********/
void FinalMergeDifferentChunks(int *data, int *buffer, const int chunk_amount, const int *chunk_size, const int *displacement)
{
	for (int step = 1; step < chunk_amount; step *= 2)
	{
		for (int i = 0; i + step < chunk_amount; i += step * 2) {
			int left = displacement[i];
			int middle = displacement[i + step] - 1;
			int right;
			if (i + 2 * step < chunk_amount)
				right = displacement[i + 2 * step] - 1;
			else
				right = displacement[chunk_amount - 1] + chunk_size[chunk_amount - 1] - 1;
			if (right >= displacement[chunk_amount - 1] + chunk_size[chunk_amount - 1])
				right = displacement[chunk_amount - 1] + chunk_size[chunk_amount - 1] - 1;
			Merge(data, buffer, left, middle, right);
		}
	}
}

int main(int argc, char** argv) {

	/********** Initialize MPI **********/
	int world_rank;
	int world_size;

	MPI_Init(0, 0);
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	/********** Create and populate the array **********/
	int n;
	int *input_array;
	if (world_rank == 0){
		scanf("%d", &n);
		input_array = malloc(n * sizeof(int));
		fprintf( stderr, "This is the unsorted array: ");
		for (int c = 0; c < n; c++) {
			scanf("%d", &input_array[c]);
			fprintf( stderr, "%d ", input_array[c]);
		}
		fprintf( stderr, "\n");
	}

	MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

	/********** Divide the array in equal-sized(if possible) chunks **********/
	int size = n / world_size;
	int rest = n % world_size;
	int *chunk_size, *displacement;

	chunk_size = malloc(world_size * sizeof(*chunk_size));
	displacement = malloc(world_size * sizeof(*displacement));
	chunk_size[0] = size + (rest > 0);
	displacement[0] = 0;
	for (int i = 1; i < world_size; ++i)
	{
		chunk_size[i] = size + (rest > i);
		displacement[i] = chunk_size[i - 1] + displacement[i - 1];
	}

	/********** Send each subarray to each process **********/
	int *chunk = malloc((size + !!rest) * sizeof(int));
	MPI_Scatterv(input_array, chunk_size, displacement, MPI_INT, chunk, chunk_size[world_rank], MPI_INT, 0, MPI_COMM_WORLD);

	/********** Perform the mergesort on each process **********/
	int *buffer = malloc((size + !!rest) * sizeof(int));
	MergeSort(chunk, buffer, 0, chunk_size[world_rank] - 1);

	/********** Gather the sorted subarrays into one **********/
	int *output_array = NULL;
	if (world_rank == 0)
		output_array = malloc(n * sizeof(int));

	MPI_Gatherv(chunk, chunk_size[world_rank], MPI_INT, output_array, chunk_size, displacement, MPI_INT, 0, MPI_COMM_WORLD);

	/********** Make the final mergeSort call **********/
	if (world_rank == 0) {

		int *buffer1 = malloc(n * sizeof(int));

		FinalMergeDifferentChunks(output_array, buffer1, world_size, chunk_size, displacement);

		/********** Display the sorted array **********/
		fprintf( stderr, "This is the sorted array: ");
		for (int c = 0; c < n; c++) {
			printf("%d ", output_array[c]);
			fprintf( stderr, "%d ", output_array[c]);
		}

		fprintf( stderr, "\n");

		/********** Clean up root **********/
		free(input_array);
		free(output_array);
		free(buffer1);

	}

	/********** Clean up rest **********/
	free(chunk);
	free(buffer);

	free(chunk_size);
	free(displacement);

	/********** Finalize MPI **********/
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();

}
