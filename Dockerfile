FROM fedora AS builder

RUN yum install -y mpich-devel redhat-rpm-config

COPY src src

RUN /usr/lib64/mpich/bin/mpicc src/app.c -o app

FROM builder as tests

RUN echo "Running tests"

RUN yum install -y diffutils

COPY tests tests

RUN cd tests; ./run_tests.sh

FROM builder as final

ENTRYPOINT /usr/lib64/mpich/bin/mpiexec -n 4 ./app
