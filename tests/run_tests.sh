set -e # break on error
mkdir tmp
cd in
for nodes in 1 2 3 4 7 24;
do
    for test_file in *.txt;
    do
        echo "Test $test_file on $nodes node(s)"
        /usr/lib64/mpich/bin/mpiexec -n "$nodes" ../../app < "$test_file" > "../tmp/$test_file"
        diff --ignore-trailing-space --ignore-blank-lines "../tmp/$test_file" "../out/$test_file"
    done;
done;