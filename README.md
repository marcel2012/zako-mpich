# ZAKO MPICH
### Local testing
To compile and execute on local environment use following commands
```
docker build -t zako-mpich .
docker run --rm zako-mpich
```

### Production execution
To run code on real computing cluster commit and push changes to this repository.
Execution results will be available [here](https://gitlab.com/marcel2012/zako-mpich/-/jobs).